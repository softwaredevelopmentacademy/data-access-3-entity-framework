﻿using System;
using System.Data.Entity;

namespace EntityFrameworkSolution
{
    public class MyDataContext : DbContext
    {
        public DbSet<Order> Orders { get; set; }
//        public DbSet<Product> Products { get; set; }

        public MyDataContext(): base("EntityFrameworkSolutionConnection")
        {
        }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);
        }
    }

    public class Order
    {
        public int Id { get; set; }
        public string ShipName { get; set; }
        public string ShipCity { get; set; }
        public DateTime OrderDate { get; set; }
        public int Freight { get; set; }
        public int ProductId { get; set; }
        public Product Product { get; set; }
    }

    public class Product
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public float UnitPrice { get; set; }
    }
}