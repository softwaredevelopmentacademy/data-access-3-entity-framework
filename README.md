# Data access - Entity Framework #

## How to use this repository? ##
All exercises we did during classes are on the branches called with pattern `exercise/x-subject`.
Please refer to presentation, those names are also used there.

## Presentation ##
You can find presentation on this repository in .pdf format, [in resources - click](resources/c-sharp_data_EF.pdf)

### How to download it? ###
![How to download presentation](resources/how-to-download-presentation.png)

# Dealing with database cleaning and connection string #

## To clean but not remove database use this script ##
[Execute this on database to remove all data and schema](https://gist.github.com/michalczukm/bc90ca7f4de9b39273d90ac1e8111a3d)

## How to find/create connection string to your database ##
![How to get connection string](resources/how-to-get-connection-string.png)
